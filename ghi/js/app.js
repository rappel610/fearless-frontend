
function createCard(name, description, pictureUrl, range, location) {
    return `
    <div class="col">
        <div class="card shadow p-3 mb-5 bg-white rounded .px-2">
            <img src=${pictureUrl} class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer text-muted">${range}</div>
        </div>
    </div>
    `;
    }
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            throw new Error('Response not ok')
        } else {
        const data = await response.json();
        // const conference = data.conferences[0];
        // const nameTag = document.querySelector('.card-title');
        // nameTag.innerHTML = conference.name;
    for(let conference of data.conferences){
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if(detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startStr = details.conference.starts;
            const start = new Date(startStr);
            const endStr = details.conference.ends;
            const end = new Date(endStr);
            const range = `${start.toLocaleDateString()} - ${end.toLocaleDateString()}`
            const location = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, range, location);
            const column = document.querySelector('.row');
            column.innerHTML += html;
        }else{
            throw new Error('Response not ok')
        }
    }

        }
    } catch (e) {
        console.error('error', e)
    }
});
